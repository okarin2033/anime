from calculator import rebase


def test_one():
    assert rebase(100, "mili", "santi") == 10
    
def test_one1():
    assert rebase(1000, "mili", "metr") == 1

def test_one2():
    assert rebase(1000000, "mili", "kilo") == 1

def test_two():
    assert rebase(10000000, "santi", "kilo") == 100

def test_two1():
    assert rebase(1000, "santi", "metr") == 10
    
def test_two2():
    assert rebase(10, "santi", "mili") == 100

def test_three():
    assert rebase(20, "kilo", "metr") == 20000

def test_three1():
    assert rebase(1, "kilo", "santi") == 100000

def test_three2():
    assert rebase(1, "kilo", "mili") == 1000000


def test_four():
    assert rebase(1, "metr", "mili") == 1000
    
def test_four1():
    assert rebase(1, "metr", "santi") == 100

def test_four2():
    assert rebase(10000, "metr", "kilo") == 10

def test_five():
    assert rebase("aboba", "horr", "pipe") == "error"

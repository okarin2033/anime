def rebase(number, inSys, outSys):
    mili = 0
    # Milimeter conversion
    if inSys == "mili":
        mili = number
    if inSys == "santi":
        mili = number * 10
    if inSys == "metr":
        mili = number * 1000
    if inSys == "kilo":
        mili = number * 1000000
    # Back conversion
    rez = 0
    if outSys == "mili":
        rez = mili
    elif outSys == "santi":
        rez = mili / 10
    elif outSys == "metr":
        rez = mili / 1000
    elif outSys == "kilo":
        rez = mili / 1000000
    else:
        return "error"
    file = open("test.txt", "a")
    file.write(f'{number} {inSys} = {rez} {outSys}\n')
    return rez
